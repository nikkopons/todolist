<?php

namespace App\Http\Controllers;

use App\Models\TaskModel;

class TasksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    public function index()
    {
        $data = TaskModel::all();
        return response($data);
        //
    }
}
